package edu.luc.etl.cs313.android.timer.test.model.time;

//import static edu.luc.etl.cs313.android.timer.common.Constants.SEC_PER_HOUR;
//import static edu.luc.etl.cs313.android.timer.common.Constants.SEC_PER_MIN;
//import static edu.luc.etl.cs313.android.timer.common.Constants.SEC_PER_TICK;
import static edu.luc.etl.cs313.android.timer.common.Constants.MAX_COUNT;
import static edu.luc.etl.cs313.android.timer.common.Constants.MAX_IDLE_TIME;
import static edu.luc.etl.cs313.android.timer.common.Constants.SEC_PER_TICK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.luc.etl.cs313.android.timer.model.time.TimeModel;

/**
 * Testcase superclass for the time model abstraction.
 * This is a simple unit test of an object without dependencies.
 *
 * @author laufer
 * @see http://xunitpatterns.com/Testcase%20Superclass.html
 */

public abstract class AbstractTimeModelTest {

    private TimeModel model;


/**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     * @param model
     */

    protected void setModel(final TimeModel model) {
        this.model = model;
    }


     /**
     * Verifies that runtime and laptime are initially 0 or less.
     */

    @Test
    public void testPreconditions() {
        assertEquals(0, model.getRuntime());
    }


/**
     * Verifies that runtime is incremented correctly.
     */
    @Test
    public void testIncrementRuntimeOne() {
        final int rt = model.getRuntime();
        model.incRuntime();
        assertEquals((rt + SEC_PER_TICK), model.getRuntime());
    }

      /**
     * Verifies that runtime turns over correctly.
     */
    //TODO - Recommended Test 7, DONE
    @Test
    public void testIncrementRuntimeToMax() {
        final int rt = model.getRuntime();
        for (int i = 0; i <= MAX_COUNT; i++) {
            model.incRuntime();
        }
        assertEquals(rt + MAX_COUNT, model.getRuntime());
    }


     /**
     * Verifies that laptime works correctly.
     */

    @Test
    public void testIdletimeOnStart() {
        final int rt = model.getRuntime();
        final int it = model.getIdleTime();
        for (int i = 0; i < MAX_IDLE_TIME; i++) {
            model.incIdleTime();
        }
        assertEquals(it + 3, model.getIdleTime());
        assertEquals(0, model.getRuntime());
    }
}

