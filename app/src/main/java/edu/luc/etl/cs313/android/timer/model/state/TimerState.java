package edu.luc.etl.cs313.android.timer.model.state;

import edu.luc.etl.cs313.android.timer.common.TimerUIListener;
import edu.luc.etl.cs313.android.timer.model.clock.OnTickListener;

/**
 * A state in a state machine. This interface is part of the State pattern.
 *
 * @author laufer
 */
interface TimerState extends TimerUIListener, OnTickListener {
    void updateView();
    int getId();
}
