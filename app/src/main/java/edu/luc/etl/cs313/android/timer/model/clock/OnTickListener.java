package edu.luc.etl.cs313.android.timer.model.clock;

/**
 * A listener for onTick events coming from the internal clock model.
 *
 * @author laufer
 */
public interface OnTickListener {
    void onTick();
}
