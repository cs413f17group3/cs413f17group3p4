package edu.luc.etl.cs313.android.timer.model.state;

import edu.luc.etl.cs313.android.timer.R;
/**
 * Created by Bradley Dabdoub on 11/12/2017.
 */

public class AlarmState implements TimerState{
    /**
     * Dynamic state machine interface for stopwatch
     * @param sm
     */

    public AlarmState(final TimerSMStateView sm) {
        this.sm = sm;
    }

    private final TimerSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionStopAlarm();
        sm.actionReset();
        sm.actionResetTick();
        sm.toStoppedState();
    }

    /**
     * current runningTime
     * @param InputCount
     */
    @Override
    public void onDisplay(Integer InputCount) {throw new UnsupportedOperationException("onAlarm");}

    @Override
    public void onTick() {
        sm.actionStartAlarm();
        sm.actionStop();    //this is stop clock!
    }


    @Override
    public void updateView() {
        sm.updateUICountTime();
    }

    @Override
    public int getId() {
        return R.string.ALARMING;
    }
}
