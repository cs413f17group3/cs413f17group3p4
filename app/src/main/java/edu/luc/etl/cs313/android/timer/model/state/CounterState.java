package edu.luc.etl.cs313.android.timer.model.state;

import edu.luc.etl.cs313.android.timer.R;
/**
 * Created by Bradley Dabdoub on 11/12/2017.
 */

public class CounterState implements TimerState {

    /**
     * Dynamic state machine interface for stopwatch
     * @param sm
     */
    public CounterState(final TimerSMStateView sm) {
        this.sm = sm;
    }

    private final TimerSMStateView sm;

    @Override
    public void onStartStop() {sm.actionIncCounter();sm.actionResetTick();}

    /**
     * current runningTime
     * @param InputCount
     */
    @Override
    public void onDisplay(Integer InputCount) {}

    @Override
    public void onTick() {
        sm.actionIncTick();
        if(sm.isTickMax())  { sm.actionBeep(); sm.toRunningState(); }  //isTickMax = 3 Timer starts to run
    }

    @Override
    public void updateView() {
        sm.updateUICountTime();
    }

    @Override
    public int getId() {
        return R.string.COUNT;
    }
}
