package edu.luc.etl.cs313.android.timer.model;

import edu.luc.etl.cs313.android.timer.common.TimerUIUpdateSource;
import edu.luc.etl.cs313.android.timer.common.TimerUIListener;


/**
 * A thin model facade. Following the Facade pattern,
 * this isolates the complexity of the model from its clients (usually the adapter).
 *
 * @author laufer
 */
public interface TimerModelFacade extends TimerUIListener, TimerUIUpdateSource {
    void onStart();
}
