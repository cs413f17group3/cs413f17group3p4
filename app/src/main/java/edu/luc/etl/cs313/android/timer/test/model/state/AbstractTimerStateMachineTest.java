
package edu.luc.etl.cs313.android.timer.test.model.state;

import static edu.luc.etl.cs313.android.timer.common.Constants.MAX_COUNT;
import static edu.luc.etl.cs313.android.timer.common.Constants.MAX_IDLE_TIME;
import static edu.luc.etl.cs313.android.timer.common.Constants.MIN_COUNT;
import static edu.luc.etl.cs313.android.timer.common.Constants.SEC_PER_TICK;
import static edu.luc.etl.cs313.android.timer.common.Constants.TIME_INC_PER_CLICK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.luc.etl.cs313.android.timer.R;
import edu.luc.etl.cs313.android.timer.common.TimerUIUpdateListener;
import edu.luc.etl.cs313.android.timer.model.clock.ClockModel;
import edu.luc.etl.cs313.android.timer.model.clock.OnTickListener;
import edu.luc.etl.cs313.android.timer.model.state.TimerStateMachine;
import edu.luc.etl.cs313.android.timer.model.time.TimeModel;


/**
 * Testcase superclass for the stopwatch state machine model. Unit-tests the state
 * machine in fast-forward mode by directly triggering successive tick events
 * without the presence of a pseudo-real-time clock. Uses a single unified mock
 * object for all dependencies of the state machine model.
 *
 * @author laufer
 * @see http://xunitpatterns.com/Testcase%20Superclass.html
 */

public abstract class AbstractTimerStateMachineTest {

    private TimerStateMachine model;

    private UnifiedMockDependency dependency;

    @Before
    public void setUp() throws Exception {
        dependency = new UnifiedMockDependency();
    }

    @After
    public void tearDown() {
        dependency = null;
    }


/**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     * @param model
     */

    protected void setModel(final TimerStateMachine model) {
        this.model = model;
        if (model == null)
            return;
        this.model.setUIUpdateListener(dependency);
        this.model.actionInit();
    }

    protected UnifiedMockDependency getDependency() {
        return dependency;
    }


/**
     * Verifies that we're initially in the stopped state (and told the listener
     * about it).
     */
    //TODO - Recommended Test 2.3 (2.1-.2 are checked in Test 1), DONE
    @Test
    public void testPreconditions() {
        assertEquals(R.string.STOPPED, dependency.getState());
    }


     /**
     * Verifies the following scenario: time is 0, press start
     * expect time 1 & state count.
     */
    //TODO - Recommended Test 3.2, DONE
    @Test
    public void testScenarioRun() {
        assertTimeEquals(0);
        // directly invoke the button press event handler methods
        model.onStartStop();
        assertTimeEquals(1);
        assertEquals(R.string.COUNT, dependency.getState());
    }

    /**
     * Verifies the following scenario: time is 0, press start 3 times
     * expect time 3 & state count. Wait 4 seconds, expect time 2 & state running.
     * Click again, expect time 0 & state stopped.
     */
    //TODO - Recommended Test 5, DONE
    @Test
    public void recommendedTest5() {
        assertTimeEquals(0);
        // directly invoke the button press event handler methods
        model.onStartStop();
        model.onStartStop();
        model.onStartStop();
        assertTimeEquals(3);
        assertEquals(R.string.COUNT, dependency.getState());
        onTickRepeat(4);
        assertTimeEquals(2);
        assertEquals(R.string.RUNNING, dependency.getState());
        model.onStartStop();
        assertTimeEquals(0);
        assertEquals(R.string.STOPPED, dependency.getState());
    }

    /**
     * Verfies the following scenario: time is zero, click 3 times and
     * wait 7 seconds. Exoect time 0 and state alarm. Click again.
     * Expect displayed value 0 and state stopped.
     */
    //TODO - Recommended Test 6, DONE
    @Test
    public void recommendedTest6() {
        assertTimeEquals(0);
        // directly invoke the button press event handler methods
        model.onStartStop();
        model.onStartStop();
        model.onStartStop();
        onTickRepeat(7);
        assertTimeEquals(0);
        assertEquals(R.string.ALARMING, dependency.getState());
        model.onStartStop();
        assertTimeEquals(0);
        assertEquals(R.string.STOPPED, dependency.getState());
    }
/**
     * Sends the given number of tick events to the model.
     *
     *  @param n the number of tick events
     */

    protected void onTickRepeat(final int n) {
        for (int i = 0; i < n; i++)
            model.onTick();
    }


     /**
     * Checks whether the model has invoked the expected time-keeping
     * methods on the mock object.
     */

    protected void assertTimeEquals(final int t) {
        assertEquals(t, dependency.getTime());
    }
}

  /**
 * Manually implemented mock object that unifies the three dependencies of the
 * stopwatch state machine model. The three dependencies correspond to the three
 * interfaces this mock object implements.
 *
 * @author laufer
 */

class UnifiedMockDependency implements TimeModel, ClockModel, TimerUIUpdateListener {
      private int runningTime = 0;
      private int tick = 0;

      @Override
      public void incIdleTime(){
          tick += SEC_PER_TICK;
      }
      @Override
      public int getIdleTime(){return tick;}
      @Override
      public void resetIdleTime(){tick = 0;}
      @Override
      public boolean isIdleTimeMax(){
          return tick >= MAX_IDLE_TIME;
      }
      @Override
      public void incRuntime() { if(!isFull()){ runningTime += TIME_INC_PER_CLICK;}}
      @Override
      public void decRuntime() {
          if(!isEmpty()){ runningTime -= SEC_PER_TICK;}
      }
      @Override
      public int getRuntime() {return runningTime;}
      @Override
      public void resetRuntime() {
          runningTime = 0;
      }
      @Override
      public boolean isFull() {return runningTime >= MAX_COUNT;}
      @Override
      public boolean isEmpty() {
          return runningTime <= MIN_COUNT;
      }
      @Override
      public void getInput(int input){runningTime = input;}

      private boolean started = false;
      private int stateId = -1;
      private int timeValue = -1;

      @Override
      public void start() {
        started = true;
      }

      @Override
      public void stop() {
        started = false;
      }

      @Override
      public void setOnTickListener(OnTickListener listener) {
          throw new UnsupportedOperationException();
      }

      @Override
      public void updateState(int stateId) {
        this.stateId = stateId;
      }

      public int getTime() {
          return timeValue;
      }

      public int getState() {
          return stateId;
      }

      public boolean isStarted() {
          return started;
      }

      @Override
      public void updateTime(int timeValue) {
        this.timeValue = timeValue;
      }

      @Override
      public void playDefaultAlarm() {

      }

      @Override
      public void stopDefaultAlarm() {

      }

      @Override
      public void playBeep() {

      }

}
